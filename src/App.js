import React from "react";
import logo from "./logo.svg";
import { BrowserRouter, Route, Switch, withRouter } from "react-router-dom";

import "./App.css";
import Register from './components/Register';
import Players from './components/Players';
import Login from './components/Login';
import NotFound from './components/NotFound';


class App extends React.Component {
  render() {
    return (
      <div className="app">
        <header className="app-header">
          <img src={logo} className="app-logo" alt="logo" />
          {/* <p>React Workshop</p> */}
        </header>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={withRouter(Players)} />
            {/* <Route path="/register" component={withRouter(Register)} />
            <Route path="/players" component={withRouter(Players)} /> */}
            {/* <Route component={NotFound} /> */}
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
