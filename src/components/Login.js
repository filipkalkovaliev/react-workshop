import React, { Component } from "react";
import PropTypes from "prop-types";
import Form from "./Form";
import AuthService from "../AuthService";
import { Link } from "react-router-dom";

class Login extends Component {
  login = data => {
    AuthService.login(data.email, data.password).then(
      res => {
        // const localStorageRef = localStorage.getItem("user");
        // if (!localStorageRef) {
        //     localStorage.setItem("user", JSON.stringify(res.user));
        // }
        this.props.history.push("/players");
      },
      res => {
        console.log(res);
      }
    );
  };
  render() {
    return (
      <div>
        <h3>Login</h3>
        <Form onSubmit={this.login} />
        <Link to="/register">Register</Link>
      </div>
    );
  }
}

export default Login;
