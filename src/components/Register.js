import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Form from './Form';
import AuthService from '../AuthService';
import {withRouter} from 'react-router-dom';
import Login from './Login';

class Register extends Component {

    register = (data) =>{
        AuthService.register(data.email,data.password).then(
            (res)=>{
                this.props.history.push("/");
            },
            (res)=>{
                console.log(res);
        })
    }

    render() {
        return (
         
             <div>
                <h3>Register</h3>
                <Form onSubmit={this.register}/>
            </div>
        );
    }
}


export default Register;