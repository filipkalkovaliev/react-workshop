import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AuthService from '../AuthService';

class Form extends Component {

    handleChange = event => {;
        //1. Take a copy of the current fish 
        this.setState({[event.target.name]:event.target.value})
      }
    submitForm = () =>{
        this.props.onSubmit(this.state);
    }
    render() {
        return (
            <div className="form-wrapper">
                <div className="form-control">
                <p>Email:</p><input type="text" name="email" onChange={this.handleChange}></input>
                </div>
                <div className="form-control">
                <p>Password:</p><input type="password" name="password" onChange={this.handleChange}></input>              
                </div>
                <button type="button" onClick={this.submitForm}>Submit</button> 
            </div>
        );
    }
}

export default Form;