import React, { Component } from "react";
import PropTypes from "prop-types";
import AuthService from "../AuthService";
import { getFunName, getRandomInt } from "../helpers";

class Players extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      players: [],
      player: null,
      error: null,
      teams: []
    };
  }

  componentDidMount() {
    // AuthService.getAuthenticationStatus(
    //   res => {
    //     this.setState({ email: res.email });
    //   },
    //   res => {}
    // );
  }

  handleChange = event => {
    this.setState({ player: event.target.value });
  };
  addPlayer = () => {
    let player = this.state.player;
    if (player) {
      const { players } = this.state;
      // players.push(player);
      this.setState({ players: players.concat(player) });
      //console.log(this.state.players);
    }
  };

  renderPlayersList = () => {
    let players = this.state.players;
    return players.map((item, index) => (
      <p key={index}>
        {index + 1}. {item}
      </p>
    ));
  };

  generateRandomTeams = () => {
    const players = [...this.state.players];
    if (players.length % 2 !== 0) {
      this.setState({ error: true });
    } else {
      const teams = [];
      const addedPlayers = [];
      for(let i = 0; i< players.length / 2; i++){
        const name = getFunName();
        const team = { name, players: []};
        while(team.players.length < 2) {
          const playerIndex = getRandomInt(0, players.length - 1);
          if(!addedPlayers.includes(players[playerIndex])) {
            team.players.push(players[playerIndex]);
            addedPlayers.push(players[playerIndex]);
          }
        }
        teams.push(team)
      }
      this.setState({ teams });
    }
  };
  renderTeams = () => {
    const teams = this.state.teams;
    return (
      <div>
        {teams.map(item => (
         
          <div key={item.name}>
            <p>
              <strong>{item.name}</strong>
            </p>
            <div>
            {item.players.map(member => (
              <p key={member}>{member},</p>
            ))}
            </div>
          </div>
        ))}
      </div>
    );
  };

  render() {
    const { players, error, teams } = this.state;
    // if (!this.state.email) return <div />;
    return (
      <div>
        <div>
          <h1>Add teams</h1>
        </div>
        <div>
          <p>Player name</p>
          <input name="player" onChange={this.handleChange} type="text" />
          <button onClick={this.addPlayer}>Add</button>
        </div>
        {players.length > 0 ? (
          <div className="players-list">
            {this.renderPlayersList()}
            <button onClick={this.generateRandomTeams}>Generate teams</button>
            {error ? <p className="error">Not enough teams</p> : <div />}
          </div>
        ) : (
          <div />
        )}
        {teams.length > 0 ? (
          <div>
            {this.renderTeams()}
          </div>
        ) : (
          <div />
        )}{" "}
      </div>
    );
  }
}

export default Players;
